Calameo Drupal Module
by @arpieb

DESCRIPTION
===========
Provides integration with the Calameo publication service. 
(http://www.calameo.com)

FEATURES
========
* Provides exportable Calameo API account definitions via CTools
* Provides a new field type Calameo publication that can be configured to pull
  publication references from any configured Calameo API account above
* Provides field formatters + TPL files for generating links and embeds for the
  publication selected
* Provides an API class that can be leveraged by other modules

DEPENDENCIES
============
* CTools

INSTALLATION
============
1. Enable module + dependencies
2. Go to Structure > Calameo and add accounts (you will need to generate API 
   keys for your account)
3. Add a new field of type Calameo publication to an entity and configure 
   which account to use from the previous step
4. Select desired field formatter under Manage display and configure it
5. (Optionally) override the provided default field formatter templates in your
   theme

TODO
====
* Add support for the API's publishing methods
* Thoroughly test the subscriber/subscription API methods

NOTES
=====
This module is not supported or provided by Calameo.  
Project sponsored by Phase2 (http://www.phase2technology.com).

